package com.pessoa.agenda.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pessoa.agenda.model.Pessoa;
import com.pessoa.agenda.repository.PessoaRepository;

@RestController
@RequestMapping(value = "/pessoa")
public class PessoaResources {

	@Autowired
	PessoaRepository repository;

	// Listar todas as pessoas
	@GetMapping("/all")
	public List<Pessoa> listaPessoas() {
		return repository.findAll();
	}

	// Lista pessoa por um id
	@GetMapping("/pessoa/{id}")
	public Pessoa listaPessoaUnica(@PathVariable(value = "id") long id) {
		return repository.findById(id);
	}

	// Adicionar Pessoa
	@PostMapping("/pessoa")
	public Pessoa adicionarPessoa(@RequestBody Pessoa pessoa) {
		return repository.save(pessoa);
	}

	// Remover Pessoa
	@DeleteMapping("/pessoa")
	public void removerPessoa(@RequestBody Pessoa pessoa) {
		repository.delete(pessoa);
	}

	//Alterar Pessoa
	@PutMapping("/pessoa")
	public Pessoa atualizarPessoa(@RequestBody Pessoa pessoa) {
		return repository.save(pessoa);
	}
}
