package com.pessoa.agenda.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pessoa.agenda.model.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

		Pessoa findById(long id);
	
}
